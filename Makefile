#
# Makefile for ChIP-Seq package
#
CC = gcc
CFLAGS = -O3 -std=gnu99 -W -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64
LDFLAGS = -O3 -std=gnu99 -fPIC -lm
#CFLAGS = -O3 -std=gnu99 -W -pedantic
#LDFLAGS = -O3 -std=gnu99 -fPIC -lm

#binDir = $(PWD)/bin
#manDir = /usr/share/man/man1/
ifeq ($(prefix),)
    prefix := $(DESTDIR).
endif
binDir = $(prefix)/bin
datDir = $(prefix)/share/chip-seq
docDir = $(prefix)/share/chip-seq/doc
manDir = $(prefix)/share/man/chip-seq/man1

CHIPSEQ_SRC = chipcor.c chipextract.c chippart.c chipcenter.c chippeak.c chipscore.c tools/compactsga.c tools/featreplace.c tools/filter_counts.c tools/countsga.c tools/bed2sga.c tools/bed2bed_display.c tools/sga2bed.c tools/sga2wig.c

PROGS = chipcor chipextract chippart chippeak chipcenter chipscore
TOOLS = tools/compactsga tools/countsga tools/featreplace tools/filter_counts tools/bed2sga tools/bed2bed_display tools/sga2bed tools/sga2wig
SCRIPTS = tools/check_bed.pl tools/chr_replace_sga.pl tools/eland2sga.pl tools/fps2sga.pl tools/gff2sga.pl tools/partit2bed.pl tools/partit2gff.pl tools/sga2fps.pl tools/sga2gff.pl
MAN_PAGES = chipcenter.1.gz chipcor.1.gz chipextract.1.gz chippart.1.gz chippeak.1.gz chipscore.1.gz
DATA_FILES = share/chr_NC_gi share/chro_idx.nstorage share/chr_size
DOC_FILES = doc/ChIP-Seq_Tools-UsersGuide.pdf
OBJS = hashtable.o

all: $(PROGS) $(TOOLS)

tools : $(TOOLS)

CHIPCOR_SRC         = chipcor.c
CHIPEXTRACT_SRC     = chipextract.c
CHIPCENTER_SRC      = chipcenter.c
CHIPPEAK_SRC        = chippeak.c
CHIPPART_SCR        = chippart.c
CHIPSCORE_SRC       = chipscore.c
COMPACTSGA_SRC      = tools/compactsga.c
COUNTSGA_SRC        = tools/countsga.c
FEATREPLACE_SRC     = tools/featreplace.c
FILTER_COUNTS_SRC   = tools/filter_counts.c
BED2BED_DISPLAY_SRC = tools/bed2bed_display.c
BED2SGA_SRC         = tools/bed2sga.c
SGA2BED_SRC         = tools/sga2bed.c
SGA2WIG_SRC         = tools/sga2wig.c

chipcenter : $(CHIPCENTER_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o chipcenter $^

chippeak : $(CHIPPEAK_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o chippeak $^

chipcor : $(CHIPCOR_SRC)
	$(CC) $(CFLAGS) -o chipcor $^

chipextract : $(CHIPEXTRACT_SRC)
	$(CC) $(CFLAGS) -o chipextract $^

chippart : $(CHIPPART_SCR)
	$(CC) $(CFLAGS) -o chippart $^

chipscore : $(CHIPSCORE_SRC)
	$(CC) $(CFLAGS) -o chipscore $^

tools/compactsga : $(COMPACTSGA_SRC)
	$(CC) $(CFLAGS) -o tools/compactsga $^

tools/countsga : $(COUNTSGA_SRC)
	$(CC) $(CFLAGS) -o tools/countsga $^

tools/featreplace : $(FEATREPLACE_SRC)
	$(CC) $(CFLAGS) -o tools/featreplace $^

tools/filter_counts : $(FILTER_COUNTS_SRC)
	$(CC) $(CFLAGS) -o tools/filter_counts $^

tools/bed2bed_display : $(BED2BED_DISPLAY_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o tools/bed2bed_display $^

tools/bed2sga : $(BED2SGA_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o tools/bed2sga $^

tools/sga2bed : $(SGA2BED_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o tools/sga2bed $^

tools/sga2wig : $(SGA2WIG_SRC) $(OBJS)
	$(CC) $(LDFLAGS) -o tools/sga2wig $^

install: install-bin

install-bin:
	mkdir -p $(binDir)
	mv $(PROGS) $(TOOLS) $(binDir)
	cp -p $(SCRIPTS) $(binDir)

install-man:
	mkdir -p $(manDir)
	cp -p $(MAN_PAGES) $(manDir) 

install-dat:
	mkdir -p $(datDir)
	cp -p $(DATA_FILES) $(datDir) 

install-doc:
	mkdir -p $(docDir)
	cp -p $(DOC_FILES) $(docDir) 

clean:
	rm -rf $(PROGS) $(TOOLS) *.o *~

uninstall:
	rm -f $(binDir)/chipcor $(binDir)/chipextract $(binDir)/chippeak $(binDir)/chippart $(binDir)/chippeak \
	$(binDir)/chipcenter $(binDir)/chipscore $(binDir)/compactsga $(binDir)/countsga $(binDir)/bed2sga $(binDir)/sga2wig \
	$(binDir)/bed2bed_display $(binDir)/featreplace $(binDir)/filter_counts $(binDir)/sga2bed $(binDir)/check_bed.pl \
	$(binDir)/chr_replace_sga.pl $(binDir)/eland2sga.pl $(binDir)/fps2sga.pl $(binDir)/gff2sga.pl $(binDir)/partit2bed.pl \
	$(binDir)/partit2gff.pl $(binDir)/sga2fps.pl $(binDir)/sga2gff.pl

uninstall-man:
	rm -rf $(manDir)/*

uninstall-dat:
	rm -rf $(datDir)

uninstall-doc:
	rm -rf $(docDir)
