#!/usr/bin/perl 

# converts fps format to sga format 
# usage: ./fps2sga.pl [-f <feature> -s <species> <-x extended SGA>] <file.fps>

use strict;
use Getopt::Long;

my %opt;
my @options = ("help", "h", "species=s", "s=s", "feature=s", "f=s", "x", "ignore0flag");

my $file = "";
my $species = "";
my $feature = "CHIP";
my $extended = 0;
my $ignore0flag = 0;

if( ! GetOptions( \%opt, @options ) ) { &Usage(); }

&Usage() if defined($opt{'help'}) || defined($opt{'h'});

&Usage() if $#ARGV < 0;


if ($opt{'f'} ne '') {
    $feature = $opt{'f'};
}
if ($opt{'feature'} ne '') {
    $feature = $opt{'feature'};
}
if ($opt{'s'} ne '') {
    $species = $opt{'s'};
}
if ($opt{'species'} ne '') {
    $species = $opt{'species'};
}
if ($opt{'x'} ne '') {
    $extended = 1;
}
if ($opt{'ignore0flag'} ne '') {
    $ignore0flag = 1;
}

$file = $ARGV[0];
#print "FPS file : $file\n";
#print "Options: feature : $feature,  Species $species\n";

#my $err_cnt = 0;
my $tot_lines = 0;

open(IN, "$file") or die ("Cannot open $file: $!\n") ;
while(<IN>){
#FP   H3K36me1_1          :+U  EU:NC_000001.9       1-    532614; 99999.
#	if (/FP   (.{20}):[-+][SMRU ]  EU:(N[TC]_\d+\.?\d*)\s+[01]([-+])\s*(\d+)(;.+)/){
    next if /#/;
    next if (!/^FP/);
    my @fps_fields = unpack('a5 a20 a5 a3 a18 a1 a1 a10 a7', $_);
    #if (/FP   (.{20}):[-+][SMRU ](\d|\s) EU:(N[TC]_\d+\.?\d*)\s+[01]([-+])\s*(\d{10})(.+)/) {
	#my $counts=(split(/_/,$1))[1];
    my $counts = "";
    my $strand = "";
    my $desc = $fps_fields[1];
    my $id = $fps_fields[4];
    my $pos = $fps_fields[7];
    $desc =~ s/\s+//g;
    $id =~ s/\s+//g;
    $pos =~ s/\s+//g;
    $counts = 1 if ($counts eq '');
    if ($ignore0flag) {
        $strand = $fps_fields[6];
    } else {
        if ($2 eq 0) {
            $strand = '0';
        } else {
            $strand = $fps_fields[6];
        }
    }
    if ($extended) {
        $desc =~ s/\s+$//;
	    print "$id\t".$feature."\t$pos\t$strand\t$counts\t$desc\n";
    } else {
        print "$id\t".$feature."\t$pos\t$strand\t$counts\n";
    }
    $tot_lines++;
}
close(IN);
#if ($err_cnt) {
#    print STDERR "\nWarning : Out of a total of $tot_lines FP lines, $err_cnt lines could not be mapped to the genome.\n\nPlease, check that the functional position reference is a genomic coordinate.\nThe accepted genome db code and entry number are the following:\n EU:N[TC]_\d+\.?\d*\n"; 
#}

sub Usage {
    print STDERR <<"_USAGE_";
    
        fps2sga.pl [options] <FPS file>

    where options are:
            -h|--help                Show this stuff
	    -f|--feature <feature>   Set Feature name <feature>
	    -s|--species <species>   Assembly <species> (i.e hg18)
	    -x                       Generate an extended SGA file with the
	                             6th field equal to the FPS 'description' field
            --ignore0flag            Ignore 0 flag at position 29 of the FP line,
                                     so keep orientation the same as original
	
_USAGE_
	exit(1);
}

1;
