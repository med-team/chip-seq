#!/usr/bin/perl

# converts chrom names into RefSeq ids within SGA  
# usage: ./chr_replace_sga.pl <-s species> -f <file.sga>

use strict;
use Getopt::Std;
use Storable qw (retrieve nstore);         # package to store persistently variables in files [http://search.cpan.org/author/AMS/Storable-2.07/Storable.pm]

my %opts;
getopt('sf:', \%opts);  # -s, & -f take arg. Values in %opts, Hash keys will be the switch names
my $DB = "/db/genome/";
#my $DB = "/home/local/db/genome/";
my $chr2SV = retrieve($DB."chro_idx.nstorage");

&Usage() unless ($opts{'f'} and $opts{'s'});

# open the SGA file
open(my $SGA, "$opts{'f'}") || die "can't open $opts{'f'} : $!";
while(<$SGA>) {
    chomp;
    if (/(\S+)\t(.*)/) {
        print "$chr2SV->{$opts{'s'}}->{$1}\t$2\n";
    }
}

close($SGA);

sub Usage {
    print STDERR <<"_USAGE_";

        chr_replace_sga.pl -s <species> -f <SGA file>

_USAGE_
    exit(1);
}

1;
